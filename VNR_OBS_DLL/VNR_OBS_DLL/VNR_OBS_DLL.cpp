// VNR_OBS_DLL.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <stdio.h>

#define SHMEMSIZE  4096
#define VNR_SHM TEXT("Local\\VNR_PlotText")
#define VNR_SHM_MUTEX TEXT("Local\\VNR_SHM_MUTEX")

HANDLE hMapObject = NULL;
LPTSTR pBuf = nullptr;
HANDLE hMutex = NULL;

__declspec(dllexport) bool SHM_initial()
{
    hMapObject = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, SHMEMSIZE, VNR_SHM);
    if (hMapObject == NULL) {
        printf("%s", "PlotTextBridge is not useable/Could not create file mapping object");
        // reopen SHM when OBS has opened it
        hMapObject = OpenFileMapping(PAGE_READWRITE, false, VNR_SHM);
        if (hMapObject == NULL)
            printf("%s", "PlotTextBridge is not useable/Could not open existence file mapping object");
    } else {
        pBuf = static_cast<LPTSTR>(MapViewOfFile(hMapObject, FILE_MAP_ALL_ACCESS, 0, 0, SHMEMSIZE));
        if (pBuf == nullptr) {
            printf("%s", "PlotTextBridge is not useable/Could not map view of file");
            hMutex = CreateMutex(NULL, false, VNR_SHM_MUTEX);
            if (hMutex == NULL)
                printf("%s", "PlotTextBridge is not useable/Could not open mutex object");
        }
    }
    return true;
}

inline __declspec(dllexport)void mutex_wait()
{
    WaitForSingleObject(hMutex, 5000);
}

inline __declspec(dllexport)void mutex_release()
{
    ReleaseMutex(hMutex);
}

inline __declspec(dllexport)void SHM_write(const void * source, size_t num)
{
    memcpy(pBuf, source, num);
}

inline __declspec(dllexport)void SHM_write_offset(size_t offset, const void * source, size_t num)
{
    memcpy(pBuf + offset, source, num);
}


inline __declspec(dllexport) void SHM_close()
{
    if (hMutex != NULL)
        CloseHandle(hMutex);
    if (pBuf != nullptr)
        UnmapViewOfFile(pBuf);
    if (hMapObject != NULL)
        CloseHandle(hMapObject);
}
