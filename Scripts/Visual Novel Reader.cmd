:: Visual Novel Reader (Debug).cmd
:: 11/1/2012 jichi
@echo off
::setlocal

cd /d %~dp0/..

title Visual Novel Reader -O
::color 8f


set PYTHON=Library\Frameworks\Python\pythonw.exe
::set PYTHON_OPT=
set PYTHON_OPT=-O
set SCRIPT=Library/Frameworks/Sakura/py/apps/reader
set SCRIPT_OPT=--nosplash

set PATH=%windir%;%windir%\system32

echo %ME%: PWD = %CD%
echo %ME%: %PYTHON% %FLAGS% %PYTHON_OPT% %SCRIPT% %SCRIPT_OPT% %*
echo.
%PYTHON% %FLAGS% %PYTHON_OPT% %SCRIPT% %SCRIPT_OPT% %*


:: EOF
